Pluros PySvc
============

PySvc is a shim used in Pluros/PlurosLite that allows multiple related
light-weight computations to be performed within a single Python process
instance without incuring the overhead of process launch and data
(de)serialization.


Installation
------------

This software is usually installed automatically through scripts. You can
manually install using pip or setup.py. Note that PySvc has to be present
inside the Pluros EFS environments and in the case of PlurosLite, it also has
to be loadable by PlurosLite. The version that PlurosLite uses must exactly
match the version inside the EFS environments.


Usage
-----

Operations to be performed are passed in as a sequence of JSON instructions
over a single dedicated SOCK_SEQPACKET socket from a parent process. The PySvc
instance terminates when the socket is closed by the parent.

The following snippets show how you can use PySvc from a Python parent process.
In the parent process, launch the pysvc instance for example via Popen, giving
it one end of a socket pair:

```
import subprocess
import socket

parent_socket, child_socket = socket.socketpair(
    family=socket.AF_UNIX, type=socket.SOCK_SEQPACKET)
subprocess.Popen(
    ['python', '-m', 'pysvc', '--verbose', str(child_sock.fileno())],
    close_fds=False)
```

You can then write instructions to the socket to get the PySvc process to do
whatever you ask. Each instruction is a JSON structure containing a callable
expression to be evaluated, a set of arguments and a set of optional output
names. The results of the callable expression are bound to the output names and
can be used in subsequent instructions.

```
inss = [
  {'f': 'sum', 'a':[[1, 2]], 'o': ['__x']},
  {'f': 'set_response', 'a': ['@__x']}
]
req = {'r': 0, 'i': inss}
parent_socket.send(json.dumps(req).encode('utf-8'))
```

The `set_response` function sets a unique return value for the entire request.
Each response is a JSON 4-tuple containing the request id, an error flag, the
return value set via set_response (null if not set) and the time taken for the
entire request. The response can be obtained in the parent process by reading
the socket.

```
id, is_error, t, value = json.loads(parent_socket.recv(100000))
```

If all went well, `id` should be `0`, `is_error` will be `False`, `value` will
be `3` and `t` won't be too large.

See test_pysvcclient.py for more examples.


Authors and Copyright
---------------------

PySvc was written by Vishal Vaidyanathan, Royal Caliber LLC.


Acknowledgements
----------------

This work was partially supported by DARPA SBIR 140D6318C0093.
