# This module provides a few supporting functions that help with quickly
# exporting Python functions via the Pluros API.
#
# Rationale
#
# Given that the Python interpreter has a long load time, it doesn't make sense
# to invoke a fresh interpreter instance for each Python call we want to run.
# Nor would it be efficient to pickle and save/load each input and output.
# Instead we provide a tiny 'service' that can call python functions and caches
# the outputs until they are requested to be serialized out of the process. The
# service is not meant to be long-running or persistent, nor is it capable of
# any concurrency. It strictly serves one request at a time.
#
# This module uses a SOCK_SEQPACKET socket for communication. The socket is
# only used for transacting small values, larger things like arrays,
# dataframes, or even large strings must be communicated via OS objects like
# files or SHM keys. For maximum compatibility with different EFS environments,
# this module should be self contained, without imports of anything other than
# python standard library stuff. This code should also be backward compatible
# with as early a version of Python as possible.
#
# The "service" is meant to be exclusively controlled by the parent process. So
# we do not listen/bind/accept etc. We expect the parent to give us a connected
# file descriptor, the number of which is passed in via the command line. The
# socket should be blocking.
#
# Each request should consist of a JSON string that respects the schema below.
# Informally, each request is a list of instructions. Each instruction contains
# a function name or expression to be called, a list of arguments and keyword
# arguments and a list of output names for storing the return values of the
# function. Each request corresponds to a single response object, that can be
# set via the set_response function.
#
# Note: no validation or sanity checking of the request is done. It is up to
# the client to format requests correctly and avoid possibly catastrophic
# errors.
#
# InsList_Schema = {
#   "type": "array",
#   "items": {
#     "type": "object",
#     "description": "an instruction to be executed",
#     "properties": {
#       "f": {
#         "type": "string",
#         "description": "a function name or expr to be called",
#       },
#       "a": {"type": "array", "description": "positional arguments"},
#       "k": {"type": "object", "description": "keyword arguments"},
#       "o": {"type": "array", "description": "names for outputs"},
#       "h": {"type": "boolean", "description": "handles errors"},
#       "p": {"type": "boolean", "description": "priority"}
#     },
#     "additionalProperties": False,
#     "required": ["fname"]
#   }
# }
#
# Request_Schema = {
#   "type": "object",
#   "properties": {
#     "r": {"type": "string|int", "description": "request id"},
#     "p": {"type": "string", "description": "priority target"},
#     "i": {"type": InsList_Schema}
#   }
# }
#
# The response has the following schema:
#
# {
#   "type": "array",
#   "items": [
#     {"type": "string", "description": "request identifier"},
#     {"type": "boolean", "description": "whether result represents an error"},
#     {"type": "float", "description": "time taken for this request"},
#     {"type": "any"}
#   ]
# }
#
#
# Values and Lifetime:
#
# Each instruction can optionally designate 1 or more output names. All names
# must be strings. Names may not begin with the vref prefix (default '@'
# character). The return value(s) of the function are bound to the specified
# names in a dictionary. There are two types of names: global names and local
# names. Local names must always begin with a double underscore. Local names
# are automatically unbound when a request is completed. Global names must be
# universally unique. i.e. once a global name has been bound to a particular
# value, that name should never be used for a different value, otherwise bad
# things can happen. One way to do this would be by generating UUIDs for
# example. Or a 64-bit counter or any other approach that practically
# guarantees uniqueness. See the priority flag description below to understand
# why global names must be unique.
#
# A global name stays bound to its value until explicitly unbound by calling
# the delete_value function. A single value may be bound to multiple names.
# The value is reclaimed by the Python Garbage Collector as usual when there
# are no remaining references to the value.
#
# Values can be referenced by name by prefixing the vref prefix to the name
# i.e. '@x' means value bound to 'x' when used as an argument in an
# instruction.
#
#
# Priority Requests:
#
# Instructions within a request are executed strictly in order. Requests are
# generally served in FIFO order, but in order to support the extraction of
# data concurrently while certain long running operations may be in progress, a
# request can optionally have a priority target. The priority target, if given,
# should be the name of a single global value. The request gets served as soon
# as the value becomes available. If the value is already available, it gets
# served immediately. This happens in a different thread than where requests
# are usually served and may happen concurrently with another request.
#
# The implementation of priority requests is intentionally simple: we examine
# the global dictionary of values. If the value already exists, we serve the
# request immediately. Otherwise we place it in a dictionary of waiting
# priority requests. When the target value is first bound, we serve the
# request.
#
# In general instructions are permitted to modify values in place. It is up to
# the client to ensure that if a value has been or is being modified in place,
# then any priority request targetting that value is sequenced after the
# modifying instruction is complete. This can be achieved by using a dummy
# output of the modifying instruction as the priority target.
#
# Note that if an instruction in an ordinary request produces a priority
# target, the priority request(s) waiting for that target will not be served
# until all instructions in the current request are complete. i.e. if there are
# multiple long running instructions within a single request, it may be some
# time before the priority request launches. This can be modified later if
# needed by allowing the suspension and resumption of a request, but right now
# seems unnecessary.


# This module provides a handful of functions to support the protocol:
#
# The make_sockets() function returns a pair of connected file descriptors. The
# second of the two is NOT marked CLOEXEC and gets passed to the pysvc process.
#
# The set_response() function can be used to set a single respone variable
# that is sent back when all instructions are complete.
#
# The process can be cleanly terminated by closing the control socket, or by
# sending SIGINT.
#
# The import_module() function can be used to import modules into the
# globals of this module.

# Note: the current implementation is such that if a priority request triggers
# another priority request that was waiting for an output of the first one, the
# latter may not run until any currently executing request completes. The
# problem originates with _set_value always pushing waiting instructions into
# the head of the queue which is only serviced by the worker thread.
#
# The behavior of a priority request triggering another request (of any kind)
# is not currently useful to us, so we're letting this slide.


import operator
import socket
import json
import sys
import os
import pickle
import traceback
import logging
import argparse
import time
import threading
import collections


# This is the maximum message size for a request or response. It should be <=
# the system limit for unix datagram messages and well above any practical size
# we need to send eval instructions.
MAX_MSG_SIZE = 32768


class Ins:
    def __init__(self, fname, args=None, kwargs=None, output_names=None,
                 handles_errors=False, method=False):
        """ Encapsulates a single function call to be executed within the
        Python process.

        Arguments:
            fname: string name of the function to be called.
            args: positional arguments to the function. References to named
              values should be strings prefixed with '@'.
            kwargs: keyword arguments to the function. See note above regarding
              references.
            output_names: names to be bound with outputs.
            handles_errors: if True, then prior results that may have failed
              are provided to the function as error Values. If False, then
              the function is never called if any of the input arguments
              are errors.
            method: if True, then the first argument should be an object and
              the function name should be the name of a method to be invoked
              upon the object.
        """
        self.fname = fname
        self.args = args if args is not None else []
        self.kwargs = kwargs if kwargs is not None else {}
        self.output_names = output_names if output_names is not None else []
        self.handles_errors = handles_errors
        self.method = method

    def to_json(self):
        """ Returns a json-equivalent structure.
        """
        ret = {'f': self.fname}
        if self.args:
            ret['a'] = self.args
        if self.kwargs:
            ret['k'] = self.kwargs
        if self.output_names:
            ret['o'] = self.output_names
        if self.handles_errors:
            ret['h'] = True
        if self.method:
            ret['m'] = True
        return ret

    @staticmethod
    def from_json(js):
        """ Convert a json-equivalent dict into an Ins object.
        """
        return Ins(
            fname=js['f'],
            args=js.get('a', []),
            kwargs=js.get('k', {}),
            output_names=js.get('o', []),
            handles_errors=js.get('h', False),
            method=js.get('m', False)
        )

    def __str__(self):
        args = ', '.join(str(arg) for arg in self.args)
        kwargs = ', '.join('%s=%s' % (k, v) for k, v in self.kwargs.items())
        onames = ', '.join(self.output_names)
        return 'Ins(fname=%s, args=%s, kwargs=%s, output_names=%s, ' \
               'handles_errors=%s, method=%s)' % (
                    self.fname, args, kwargs, onames, self.handles_errors,
                    self.method)


class Value:
    """ Shallow wrapper for all the values we cache in this service.
    """
    def __init__(self, v, is_error=False):
        self.v = v
        self.is_error = is_error


# This is tailored for control by a single parent process. The parent must
# pass in the file descriptor number of one socket from a connected pair
# of sockets (e.g. using socketpair
class ServerSocket:
    """ This is a Unix Datagram Socket that returns one request at a time.
    The format of the incoming message matches the implementation of
    ClientSocket in pysvcclient.py.
    """

    # Since we only allow one request at a time and no out-of-order responses,
    # there is no request identifier.
    class Request:
        """ A request consists of a list of instructions.
        """
        def __init__(self, id, inss, prio=None):
            self.id = id
            self.inss = inss
            self.prio = prio
            # Default response: not an error, None
            self.response = (False, None)

    def __init__(self, fd):
        """ Initialize a wrapper around the socket endpoint `fd`.
        """
        self._socket = socket.socket(fileno=fd)

    def close(self):
        """ Close the socket.
        """
        self._socket.close()

    def get_request(self, logger=None):
        """ Blocks until a request is available and returns the request.
        """
        if logger:
            logger.debug('waiting for request')
        msg = self._socket.recv(MAX_MSG_SIZE)
        # Check for eof.
        if len(msg) == 0:
            if logger:
                logger.debug('zero-sized message, signaling EOF')
            raise EOFError()
        if logger:
            logger.debug('request: %s', msg)
        t0 = time.time()
        req = json.loads(msg)
        id = req.get('r', None)
        inss = [Ins.from_json(ins) for ins in req['i']]
        prio = req.get('p', None)
        t1 = time.time()
        if logger:
            logger.debug('parsed %d instructions in %s seconds'
                         % (len(inss), t1 - t0))
        return self.Request(id, inss, prio)

    # Should there be one response per instruction? Maybe...
    def send_reply(self, id, is_error, t, response):
        """ Sends a response to the requester at `addr` for request with id
        `id`.
        """
        # Package response along with the id.
        try:
            msg = json.dumps((id, is_error, t, response)).encode('utf-8')
        except Exception as ex:
            # If we could not convert response to json, send this error
            # instead.
            msg = json.dumps((id, True, t, '%s' % ex)).encode('utf-8')
        # Send it back to the requester.
        self._socket.send(msg)


class Queue:
    """ Simple single-producer, single-consumer FIFO queue.
    """
    def __init__(self, lock=None):
        # Condition variable for queue non-empty notification.
        self._cv = threading.Condition(lock=lock)
        # Shared queue
        self._reqs = collections.deque()

    def push(self, req):
        """ Add an instruction to the end of the queue. This should only
        be called from the primary thread. Pushing a value of None will
        cause the consumer thread to stop running.
        """
        with self._cv:
            self._reqs.append(req)
            # Note: Python requires lock held while calling notify.
            if len(self._reqs) == 1:
                self._cv.notify()

    def pop(self):
        """ Returns the first instruction in the queue, blocking the calling
        thread until an instruction is available if the queue is empty.
        """
        with self._cv:
            # If there's nothing, we have to block.
            while len(self._reqs) == 0:
                self._cv.wait()
            return self._reqs.popleft()

    def insert_nolock(self, reqs):
        """ Inserts the requests in `reqs` into the front of the queue. This
        function assumes that the lock is already held and does not acquire
        the lock.
        """
        self._reqs.extendleft(reqs)


# Typed wrapper around a list. Stores a bunch of priority requests that are
# waiting for a particular value to become ready.
class WaitList(list):
    pass


class Service:
    """ Runs a minimal 'service' that listens for requests on a unix datagram
    socket and performs the instructions that are sent in each request.

    The service instance is available as g_service in the globals of this
    module.
    """
    def __init__(self, socket_fd, vref_prefix='@', logger=None):
        # This is the global dictionary that is used to cache outputs from this
        # service.
        self._values = {}

        # prefix identifying string arguments as variable references.
        self.vref_prefix = vref_prefix

        # Create socket wrapper.
        self._sock = ServerSocket(socket_fd)

        # Logger only if debug tracing requested.
        self.logger = logger

        # This flag is set if we failed an instruction because an input
        # argument was previously set to an error. This is used to avoid
        # redundant displays of exception tracebacks in verbose mode.
        self._failed_due_to_previous_exception = False

        # Lock for global values.
        self._lock = threading.Lock()

        # Instruction queue.
        self._queue = Queue(lock=self._lock)

    def delete_value(self, name):
        try:
            with self._lock:
                del self._values[name]
        except KeyError:
            if self.logger:
                self.logger.debug('delete_value: %s not present' % name)

    def set_response(self, req, v, *args):
        """ Set the response value that is to be returned to sender. Only for
        really small stuff like integers, floats, short strings etc. Anything
        too large will cause a failure.

        Errors are sent as string descriptions along with a flag indicating
        it is an error.

        Additional arguments can be specified to this function: if any of
        them is an error, the first error is returned. Otherwise, they are
        ignored and only v is returned as a response.
        """
        # Check for errors in any of v and args.
        for x in (v, *args):
            if isinstance(x, Value) and x.is_error:
                ex = ''.join(traceback.format_exception(
                    type(x.v), x.v, x.v.__traceback__, limit=5))
                req.response = (True, ex)
                return
        req.response = (False, v.v if isinstance(v, Value) else v)

    def pickle_dump(self, locals, name, path, **kwargs):
        """ Dumps the Value with the given name using pickle to the given path.
        """
        if name.startswith('__'):
            v = locals[name]
        else:
            with self._lock:
                v = self._values[name]
        with open(path, 'wb') as f:
            pickle.dump(v, f, **kwargs)

    def pickle_load(self, locals, path, name):
        """ Loads the value that was saved with pickle_dump.
        """
        with open(path, 'rb') as f:
            v = pickle.load(f)
        if name.startswith('__'):
            locals[name] = v
        else:
            with self._lock:
                self._set_value(name, v, locals)

    # Run each instruction in the request.
    def _handle_request(self, req):
        # Temporary values.
        locals = {}
        # Run all the instructions in sequence.
        t0 = time.time()
        for ins in req.inss:
            self._run_ins_safe(req, ins, locals)
        # Send response.
        t1 = time.time()
        self._sock.send_reply(
            req.id, req.response[0], t1 - t0, req.response[1])
        t2 = time.time()
        if self.logger:
            logger.debug('completed batch in %s + %s seconds'
                         % (t1 - t0, t2 - t1))

    # This runs in a separate thread handling ordinary requests.
    def _run_worker(self):
        while True:
            req = self._queue.pop()
            if req is None:
                break
            self._handle_request(req)

    # Run the main loop, fetching instructions from the stream p and placing
    # them in the queue. Priority instructions are immediately executed in this
    # queue (if the target is available) or placed into a wait list for the
    # execution thread to handle at a later time.
    def run(self):
        # Start the thread that processes normal (non-priority) requests.
        worker = threading.Thread(target=self._run_worker)
        worker.start()

        # This runs until the controlling socket is closed.
        while True:
            try:
                req = self._sock.get_request(self.logger)
            except KeyboardInterrupt:
                # Got a SIGINT, terminate.
                break
            except EOFError as ex:
                # The control side closed. We treat this as equivalent to
                # getting a stop instruction.
                break
            except OSError as ex:
                # If there was some problem with the connection etc. we
                # have to stop.
                raise
            except Exception as ex:
                # Reply if we got a bad message
                self._sock.send_reply(True, str(ex))
                continue

            # If this is a normal request, place it in the queueu.
            if req.prio is None:
                self._queue.push(req)
            # otherwise handle the priority request.
            else:
                # We have to do this with the lock held.
                run_now = False
                with self._lock:
                    v = self._values.setdefault(req.prio, WaitList())
                    # If we just introduced a wait list, or if there was an
                    # earlier one, we add this request to the list.
                    if isinstance(v, WaitList):
                        if self.logger:
                            self.logger.debug(
                                'priority request %s waiting on %s'
                                % (req.id, req.prio))
                        v.append(req)
                    # Otherwise the value is already present.
                    else:
                        # Ensure this is a Value - internal logic check.
                        assert isinstance(v, Value)
                        # Run the instruction right now, but after giving up
                        # the lock.
                        run_now = True
                if run_now:
                    if self.logger:
                        self.logger.debug(
                            'handling priority request %s' % req.id)
                    # Do not allow thread to switch. This assumes the python
                    # implementation will still yield the thread if it fails to
                    # acquire a lock.
                    st_prev = sys.getswitchinterval()
                    sys.setswitchinterval(10)
                    try:
                        self._handle_request(req)
                    finally:
                        sys.setswitchinterval(st_prev)

        if self.logger:
            self.logger.debug('main loop complete, waiting for worker')
        # Shutdown the worker by sending a None instead of a request.
        self._queue.push(None)
        worker.join()
        if self.logger:
            self.logger.debug('worker finished, terminating')

    # All this is a super stripped-down version of the stuff in execruntime.
    def _run_ins_safe(self, req, ins, locals):
        # Reset flag so we'll know whether or not to dump traceback.
        self._failed_due_to_previous_exception = False
        try:
            if self.logger:
                self.logger.debug('running: %s', ins)
            self._run_ins(req, ins, locals)
        except Exception as ex:
            if self.logger:
                if self._failed_due_to_previous_exception:
                    self.logger.debug('failed due to previous error:', ex)
                else:
                    msg = ''.join(traceback.format_exception(
                        type(ex), ex, ex.__traceback__))
                    self.logger.debug('exception: %s', msg)
            with self._lock:
                for name in ins.output_names:
                    self._set_value(name, Value(ex, is_error=True), locals)

    def _is_vref(self, arg):
        return isinstance(arg, str) and arg.startswith(self.vref_prefix)

    # This should be called with the lock held by the caller.
    def _get_value(self, ref, locals, handles_errors=False):
        name = ref[len(self.vref_prefix):]
        d = locals if name.startswith('__') else self._values
        try:
            ret = d[name]
        except KeyError:
            raise RuntimeError("value '%s' does not exist" % name)
        if handles_errors:
            return ret
        elif ret.is_error:
            self._failed_due_to_previous_exception = True
            raise ret.v
        else:
            return ret.v

    # Get an argument, looking it up in values if needed. If the value recorded
    # previously is an exception, raise it.
    def _get_arg(self, arg, locals, handles_errors=False):
        if self._is_vref(arg):
            return self._get_value(arg, locals, handles_errors)
        else:
            return arg

    # Resolve value references, check for errors and return args and kwargs.
    # The lock should be held by the caller when calling this.
    def _gather_arguments(self, ins, locals, handles_errors=False):
        args = [self._get_arg(v, locals, handles_errors) for v in ins.args]
        kwargs = {k: self._get_arg(v, handles_errors)
                  for k, v in ins.kwargs.items()}
        return args, kwargs

    # Sets a value in the global table. Assumes the lock is held by the caller.
    def _set_value(self, name, value, locals):
        # Local values cannot serve as priority request targets, so we keep it
        # really simply for locals.
        if name.startswith('__'):
            locals[name] = value
            return

        # Check if there is a wait list for this value.
        wl = self._values.get(name, None)
        if isinstance(wl, WaitList):
            if self.logger:
                self.logger.debug(
                    'value %s triggers %d priority requests' % (name, len(wl)))
            # Push these instructions to the head of the queue.
            # Python locks are recursive by default, so this is ok
            # to do with the lock already held.
            self._queue.insert_nolock(wl)

        # Bind the value.
        self._values[name] = value

    def _run_ins(self, req, ins, locals):
        t0 = time.time()
        with self._lock:
            args, kwargs = self._gather_arguments(ins, locals,
                                                  ins.handles_errors)
        t1 = time.time()

        # ---------------------------------------------------------------------
        # Some predefined functions need additional context (request, locals,
        # etc.) but the client cannot specify such things. So we define these
        # closures in the local scope.

        def set_response(*args):
            self.set_response(req, *args)

        def pickle_dump(name, path, **kwargs):
            self.pickle_dump(locals, name, path, **kwargs)

        def pickle_load(path, name):
            self.pickle_load(locals, path, name)

        # --------------------------------------------------------------------

        # The function can also be a value reference. It should be a callable.
        if self._is_vref(ins.fname):
            with self._lock:
                f = self._get_arg(ins.fname, locals)
        else:
            # If this is a method, we look it up on the first argument, which
            # should be a positional.
            if ins.method:
                f = getattr(args.pop(0), ins.fname)
            else:
                # We eval because that allows lambdas which is very convenient.
                # But may be pretty slow even when this just a global variable.
                # We could try to look it up as a dot-separated identifier
                # before eval'ing...
                f = eval(ins.fname)
        t2 = time.time()
        out = f(*args, **kwargs)
        t3 = time.time()
        if len(ins.output_names) == 0:
            # We are discarding the output
            pass
        elif len(ins.output_names) == 1:
            # Assign whatever was returned to the name.
            with self._lock:
                self._set_value(ins.output_names[0], Value(out), locals)
        else:
            # Map output names to returned values as a sequence.
            if len(ins.output_names) != len(out):
                raise TypeError('%s: expected %d outputs, got %d'
                                % (ins.fname, len(ins.output_names), len(out)))
            # Instructions waiting on one of the outputs, if any.
            with self._lock:
                for name, val in zip(ins.output_names, out):
                    self._set_value(name, Value(val), locals)

        t4 = time.time()
        if logger:
            logger.debug('%s: args:%s eval:%s call:%s store:%s'
                         % (ins.fname, t1 - t0, t2 - t1, t3 - t2, t4 - t3))

    def close(self):
        """ Close and cleanup.
        """
        self._sock.close()


# Single global service instance.
g_service = None


def delete_value(name):
    """ Unbinds the given name.
    """
    g_service.delete_value(name)


def import_module(*names):
    """ Imports the given modules into the global namespace.
    """
    for name in names:
        prefix = name.split('.', 1)[0]
        globals()[prefix] = __import__(name)


def load_module_source(name, source):
    """ Loads a module from source.

    Currently name cannot have periods. The loaded module is placed in both
    sys.modules and globals.
    """
    import sys
    import importlib.util
    spec = importlib.util.spec_from_loader(name, loader=None)
    mod = importlib.util.module_from_spec(spec)
    exec(source, mod.__dict__)
    globals()[name] = sys.modules[name] = mod


def typename(v):
    """ Returns the full name of the type of a value.
    """
    t = type(v)
    try:
        return t.__module__ + '.' + t.__qualname__
    # Older pythons than 3.3
    except AttributeError:
        return t.__module__ + '.' + t.__name__


def identity(x):
    """ Returns the input.
    """
    return x


def raise_(ex):
    """ Raises the given exception, useful within lambdas.
    """
    raise ex


# The service can be started by invoking python -m pysvc path_to_socket
if __name__ == '__main__':
    ap = argparse.ArgumentParser(description='Run a pysvc service instance')
    ap.add_argument('--verbose', action='store_true', default=False,
                    help='logs the execution of each instruction')
    ap.add_argument('--vref-prefix', default='@',
                    help='string prefix for value references')
    ap.add_argument('socket_fd', type=int,
                    help='integer fd of UNIX/SEQ_PACKET socket to use')
    opts = ap.parse_args()
    if opts.verbose:
        # Match pluros/pluroslite logs to the extent possible.
        logging.addLevelName(logging.DEBUG, 'T')
        logging.addLevelName(logging.INFO, 'I')
        logging.addLevelName(logging.WARNING, 'W')
        logging.addLevelName(logging.ERROR, 'E')
        logging.basicConfig(level=logging.DEBUG)
        logger = logging.getLogger('pysvc %d' % os.getpid())
        logger.propagate = False
        handler = logging.StreamHandler(stream=sys.stderr)
        formatter = logging.Formatter(
            fmt='%(levelname)s: '
                + '%(asctime)s.%(msecs)03d %(name)-12s(%(lineno)4s): '
                + '%(threadName)s: '
                + '%(message)s',
            datefmt='%Y%m%d %H:%M:%S')
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    else:
        logger = None
    g_service = Service(opts.socket_fd, opts.vref_prefix, logger=logger)
    g_service.run()
    g_service.close()
