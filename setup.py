from setuptools import setup


setup(
  name='pluros-pysvc',
  version='1.1.0',
  author='Vishal',
  author_email='vishal@royal-caliber.com',
  description='Python service wrapper for Pluros',
  py_modules=['pysvc'],
)
