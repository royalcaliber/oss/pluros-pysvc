import unittest
import pysvc
import tempfile
import os
import subprocess
import signal
import time
import socket
import json
import math
import atexit


# Ensure any subprocesses are terminated.
g_procs = set()


# Note: this is for tests only. In real usage, the first descriptor should be
# CLOEXEC!
def make_sockets():
    sock0, sock1 = socket.socketpair(
        family=socket.AF_UNIX, type=socket.SOCK_SEQPACKET)
    # From 3.4 onwards files are CLOEXEC by default and have to be marked
    # inheritable.
    try:
        os.set_inheritable(sock1.fileno(), True)
    except AttributeError:
        pass
    return sock0, sock1


def kill_all_procs():
    if g_procs:
        print("cleaning up %d procs" % len(g_procs))
    for proc in g_procs:
        proc.kill()
        proc.wait()


atexit.register(kill_all_procs)


class TestSvc(unittest.TestCase):
    def setUp(self):
        self.socket, child_sock = make_sockets()
        self.proc = subprocess.Popen(
            ['python', '-m', 'pysvc', '--verbose', str(child_sock.fileno())],
            close_fds=False
        )
        g_procs.add(self.proc)
        # Request counter to provide unique request ids.
        self.rc = 0
        child_sock.close()

    def tearDown(self):
        self.socket.close()
        try:
            ret = self.proc.wait(timeout=2)
        except subprocess.TimeoutExpired:
            self.proc.kill()
            ret = self.proc.wait()
        g_procs.remove(self.proc)
        self.assertEqual(ret, 0)

    def send_request(self, req):
        msg = json.dumps(req).encode('utf-8')
        self.socket.send(msg)

    def recv_response(self):
        resp = self.socket.recv(pysvc.MAX_MSG_SIZE)
        return json.loads(resp)

    def request(self, inss, prio=None):
        req = {'r': self.rc, 'i': inss}
        self.rc += 1
        self.send_request(req)
        id, is_error, t, value = self.recv_response()
        assert id == req['r']
        if is_error:
            raise RuntimeError(value)
        else:
            return value

    # Just test startup and shutdown.
    def test_terminate(self):
        # Send SIGINT and ensure it quits. Have to wait until the signal
        # handler is installed though.
        time.sleep(0.5)
        self.proc.send_signal(signal.SIGINT)
        pass

    # Test setting a value and getting back a response
    def test_set_and_get(self):
        inss = [
            {'f': 'sum', 'a': [[1, 2]], 'o': ['x']},
            {'f': 'set_response', 'a': ['@x']},
        ]
        resp = self.request(inss)
        self.assertEqual(resp, 3)

    # Test module imports
    def test_import(self):
        inss = [
            {'f': 'import_module', 'a': ['math']},
            {'f': 'math.sin', 'a': [0.5], 'o': ['x']},
            {'f': 'set_response', 'a': ['@x'], 'h': True},
        ]
        resp = self.request(inss)
        self.assertAlmostEqual(resp, math.sin(0.5))

    # Test method call
    def test_method(self):
        inss = [
            {'f': 'import_module', 'a': ['datetime']},
            {'f': 'datetime.date.today', 'o': ['x']},
            {'f': 'ctime', 'a': ['@x'], 'o': ['y'], 'm': True},
            {'f': 'set_response', 'a': ['@y'], 'h': True},
        ]
        resp = self.request(inss)
        self.assertTrue(isinstance(resp, str))

    # Test callable reference
    def test_callable(self):
        inss = [
            {'f': 'eval', 'a': ['int'], 'o': ['x']},
            {'f': '@x', 'a': ['10'], 'o': ['y']},
            {'f': 'set_response', 'a': ['@y'], 'h': True},
        ]
        resp = self.request(inss)
        self.assertEqual(resp, 10)

    # Test error handling
    def test_error_handling(self):
        inss = [
            {'f': 'lambda x: x / 0', 'a': [1], 'o': ['x']},
            {'f': 'set_response', 'a': [None, '@x'], 'h': True}
        ]
        with self.assertRaises(RuntimeError):
            resp = self.request(inss)

    # Test pipelining. These requests should be executed in order always.
    def test_pipeline(self):
        req0 = {'r': 0, 'i': [{'f': 'lambda: time.sleep(0.5)'}]}
        self.send_request(req0)
        req1 = {'r': 1, 'i': [{'f': 'lambda: time.sleep(0.1)'}]}
        self.send_request(req1)
        id0, *_ = self.recv_response()
        id1, *_ = self.recv_response()
        self.assertEqual(id0, 0)
        self.assertEqual(id1, 1)

    # Test priority request.
    def test_priority(self):
        req0 = {'r': 0, 'i': [{'f': 'lambda: 10', 'o': ['x']}]}
        self.send_request(req0)
        req1 = {'r': 1, 'i': [{'f': 'lambda: time.sleep(1)'}]}
        self.send_request(req1)
        req2 = {'r': 2, 'i': [{'f': 'lambda: 0'}], 'p': 'x'}
        self.send_request(req2)
        id0, *_ = self.recv_response()
        id1, *_ = self.recv_response()
        id2, *_ = self.recv_response()
        # This order is not guaranteed, but is very likely. Of course it can be
        # completely thrown off if you are running a debugger in one of the
        # threads...
        self.assertEqual(id0, 0)
        self.assertEqual(id1, 2)
        self.assertEqual(id2, 1)

    # Test priority request when the value is not yet ready. Again order here
    # is not guaranteed, but very likely.
    def test_priority(self):
        req0 = {'r': 0, 'i': [{'f': 'lambda: time.sleep(1)', 'o': ['x']}]}
        req1 = {'r': 1, 'i': [{'f': 'lambda: time.sleep(2)'}]}
        req2 = {'r': 2, 'i': [{'f': 'lambda: 0'}], 'p': 'x'}
        self.send_request(req0)
        self.send_request(req1)
        self.send_request(req2)
        id0, *_ = self.recv_response()
        id1, *_ = self.recv_response()
        id2, *_ = self.recv_response()
        self.assertEqual(id0, 0)
        self.assertEqual(id1, 2)
        self.assertEqual(id2, 1)


if __name__ == '__main__':
    unittest.main()
